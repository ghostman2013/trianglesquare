﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleSquare;

namespace TriangleSquareTest
{
    [TestClass]
    public class TriangleMathTest
    {
        [DataRow(0f, 1f, 1f, -1f)]
        [DataRow(1f, 0f, 1f, -1f)]
        [DataRow(1f, 1f, 0f, -1f)]
        [DataRow(3f, 4f, 5f, 6f)]
        [DataRow(4f, 3f, 5f, 6f)]
        [DataRow(5f, 3f, 4f, 6f)]
        [DataRow(5f, 2f, 4f, -1f)]
        [TestMethod]
        public void TestSquare(float a, float b, float c, float square)
        {
            Assert.AreEqual(square, TriangleMath.Squre(a, b, c), 0.001f);
        }
    }
}
