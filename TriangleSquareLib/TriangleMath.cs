﻿namespace TriangleSquare
{
    public abstract class TriangleMath
    {
        /// <summary>
        /// Private constructor to prevent inheritance
        /// </summary>
        private TriangleMath() { }

        /// <summary>
        /// Returns square of the right triangle
        /// </summary>
        /// <param name="a">Length of first side.</param>
        /// <param name="b">Length of second side.</param>
        /// <param name="c">Length of third side.</param>
        /// <returns>Triangle square or negative value (-1f) if triangle is not right.</exception>
        public static float Squre(float a, float b, float c)
        {
            if (a <= 0.0f || b <= 0.0f || c <= 0.0f)
            {
                return -1f;
            }

            float h, k1, k2;

            if (a > b)
            {
                h = a;
                k2 = b;
            }
            else
            {
                h = b;
                k2 = a;
            }

            if (c > h)
            {
                k1 = h;
                h = c;
            }
            else
            {
                k1 = c;
            }

            if (h * h != k1 * k1 + k2 * k2)
            {
                return -1f;
            }

            return 0.5f * k2 * k1;
        }
    }
}
